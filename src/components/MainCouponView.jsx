var React = require('react');
var CouponList = require('./CouponList.jsx');
var CouponDetails = require('./CouponDetails.jsx');
var Actions = require('../reflux/company_actions.jsx');
var companyStore = require('../reflux/company_store.jsx');
var Reflux = require('reflux');

var MainCouponView = React.createClass({
        mixins : [Reflux.listenTo(companyStore, 'onCouponChange')],

        getInitialState : function () {
            return {coupons: [],
                showSingle: false,
                currentCoupon: null}
        },

        onCouponChange: function (event, couponData) {
            this.setState({
                coupons: couponData
            });
        },

        componentWillMount : function () {
            Actions.getCompanyCoupons();
        },

        onChildClicked: function (coupon) {

            console.log("MainCouponView callback");
            var newState = {
                showSingle: true,
                currentCoupon: coupon
            };

            this.setState(newState);
        },

        onReturnToList: function () {
            console.log("MainCouponView goingBack callback");
            var newState = {
                showSingle: false
            };

            this.setState(newState);
        },

        renderList: function () {

            return (
                <CouponList coupons={this.state.coupons} childCallback={this.onChildClicked}/>
            );
        },

        renderDetails: function () {

            return (
                <CouponDetails userData={this.props.userData} coupon={this.state.currentCoupon} childCallback={this.onReturnToList} />
            );
        },

        render: function () {

            if (this.state.showSingle) {
                return this.renderDetails();
            }
            else
            {
                return this.renderList();
            }
        }
    })
    ;

module.exports = MainCouponView;


allCoupons = [
    {
        "amount": 13,
        "endDate": "2017-03-01",
        "id": 1,
        "imagePath": "poster.jpg",
        "message": "Kosher clock (tm).\nIt's a beautiful designed wall clock which has some smart features that will help you stay\nsaint. It knows when Sabbath starts and ends, and notifies you about it by simply stopping.\n(No electronics are allowed in Sabbath, duh!)",
        "price": 7.91,
        "startDate": "2016-02-26",
        "title": "Kosher clock",
        "type": "ELECTRONICS"
    },
    {
        "amount": 88,
        "endDate": "2018-03-14",
        "id": 2,
        "imagePath": "poster.jpg",
        "message": "poster.jpg",
        "price": 1.54,
        "startDate": "2016-02-26",
        "title": "High Tech Ice",
        "type": "ELECTRONICS"
    },
    {
        "amount": 547,
        "endDate": "2017-06-20",
        "id": 3,
        "imagePath": "poster.jpg",
        "message": "JustPhone (tm)\nThere is a saying, the new one is the old one. So, let me just break it to you.\nJustPhone (tm) - here for you anytime, you can JUST call you're best buddy,\nsimple as that. No intervations by I.M messengers, no intervations by non stopping\nnotifications from your social networks. JUST PHONE - It's good for you.",
        "price": 57.47,
        "startDate": "2016-02-26",
        "title": "JustPhone",
        "type": "ELECTRONICS"
    },
    {
        "amount": 74,
        "endDate": "2016-08-23",
        "id": 4,
        "imagePath": "poster.jpg",
        "message": "Smiley Cup, is here for you.\nThere is nobody to talk to? Here is your Smiley Cup.\nJust look at it, it's smiles back and your mood is suddenly all better.\nSmiley Cup going to make everything work for you. ",
        "price": 4.0,
        "startDate": "2016-02-26",
        "title": "Smiley Cup",
        "type": "ELECTRONICS"
    },
    {
        "amount": 111,
        "endDate": "2017-09-12",
        "id": 5,
        "imagePath": "poster.jpg",
        "message": "Adapter that works just for eveything.\nGoing to a meeting and suddenly out of battery when you need to make a\ngreat decision? And you don't have the right cable with you? Don't you worry - \nThe Adapter (TM) is here for you. Just put it on - and suddenly everything is clicking.\nAnd so on... ",
        "price": 35.74,
        "startDate": "2016-02-26",
        "title": "Adapter",
        "type": "ELECTRONICS"
    },
    {
        "amount": 332,
        "endDate": "2016-09-21",
        "id": 6,
        "imagePath": "poster.jpg",
        "message": "It's the tastiest hummus you'll ever ate.\nMade by Jews obeing citizens from Judea and Samaria. Buy the Hummus,\nsupport them and break the sanctions! ",
        "price": 10.88,
        "startDate": "2016-02-26",
        "title": "Hummus",
        "type": "FOOD"
    },
    {
        "amount": 500,
        "endDate": "2016-06-16",
        "id": 7,
        "imagePath": "poster.jpg",
        "message": "Fries at McGills!\nWe offer all sort of fries, American style, French style, Iranian style and so on.\nThere is more to fries than you can imagine. Come with this coupone to us and it'll \nserve you two kind of sousages from the menu + a beer.\nBon appetite! ",
        "price": 15.5,
        "startDate": "2016-02-26",
        "title": "Fries at McGills",
        "type": "FOOD"
    },
    {
        "amount": 1000,
        "endDate": "2016-11-01",
        "id": 8,
        "imagePath": "poster.jpg",
        "message": "HambuPizza is the whole new world. \nCome and try it, you will not regret it.\nNow for the half price!\nMade for sharing... \nEnjoy.",
        "price": 21.21,
        "startDate": "2016-02-26",
        "title": "HambuPizza",
        "type": "FOOD"
    },
    {
        "amount": 999,
        "endDate": "2017-05-22",
        "id": 10,
        "imagePath": "poster.jpg",
        "message": "The \"Half for Free\" you're beloved supermarket coming with a new crazy sale!!!\nBuy this coupon and get 50% discount at the cash desk, ON EVERYTHING!\nSo come on, what are you waiting for?! GO AHEAD.",
        "price": 99.99,
        "startDate": "2016-02-26",
        "title": "Shopping at Half For Free",
        "type": "FOOD"
    },
    {
        "amount": 100,
        "endDate": "2016-07-19",
        "id": 11,
        "imagePath": "poster.jpg",
        "message": "Wonderful and tasty dinner just for two at ToothFairy.\nDon't you worry, you're not going to break a tooth.\nThe food will melt in your mouth, and for that great price you'll get two starters,\ntwo main dishes, drinks on the house (fulfill for free) and a cake.\nSee you!",
        "price": 9.99,
        "startDate": "2016-02-26",
        "title": "Full dinner just for two at ToothFairy",
        "type": "FOOD"
    },
    {
        "amount": 5000,
        "endDate": "2016-09-01",
        "id": 12,
        "imagePath": "poster.jpg",
        "message": "Skinny Cow Ice Cream will make you stay as thin as you are.\nOn a diet and craving for an ice cream? No problem! There is a solution for you.\nOur ice cream is just made for you, we care.\n",
        "price": 5.0,
        "startDate": "2016-02-26",
        "title": "Skinny Cow Ice Cream",
        "type": "HEALTH"
    },
    {
        "amount": 600,
        "endDate": "2021-03-10",
        "id": 13,
        "imagePath": "poster.jpg",
        "message": "Pulse Me (TM)\nNew device that helps you stay in touch with yourself wherever you go.\nNo need to see a doctor, just a quick check and you'll on it.\nThere is an option to extend the support and purchase additional kit,\nfor details click here: https://pulse.me\nCustomer service: 1800-547-PULSE-ME",
        "price": 66.66,
        "startDate": "2016-02-26",
        "title": "Pulse me ",
        "type": "HEALTH"
    },
    {
        "amount": 5476,
        "endDate": "2018-08-13",
        "id": 14,
        "imagePath": "poster.jpg",
        "message": "Vitamins for you soul.\nApproved by GodAssosiation.\nIt's just good for you soul, it just is. \nTrust us and you will ne pleased.\n+1 to Karma.\nAmen.\n",
        "price": 33.77,
        "startDate": "2016-02-26",
        "title": "Vitamins for your soul ",
        "type": "HEALTH"
    },
    {
        "amount": 666,
        "endDate": "2017-03-21",
        "id": 15,
        "imagePath": "poster.jpg",
        "message": "Uganda Insurance, be sure for your family health.\nThat insurance is not what you think off, we will give you a real deal. *\n* dentist insurance is not included\n* brain damages are not included\n* psychological problems are not included\n* headache is not covered\nWe care about you. Joins us today before it's too late: \nUGANDA-INSURANCE-911 ",
        "price": 11.0,
        "startDate": "2016-02-26",
        "title": "Uganda Insurance",
        "type": "HEALTH"
    },
    {
        "amount": 998,
        "endDate": "2016-06-27",
        "id": 16,
        "imagePath": "poster.jpg",
        "message": "The Watchers, exclusively team that WILL watch what you are eating.\nWe are here for you.  We designed a whole new system that will prevent you\nfrom gaining weight.\nWe fight obesity every single day. All you need to do is to buy this coupon and join\na local team of \"The Watchers\".\n",
        "price": 214.54,
        "startDate": "2016-02-26",
        "title": "Join today The Watchers",
        "type": "HEALTH"
    },
    {
        "amount": 54,
        "endDate": "2020-09-20",
        "id": 17,
        "imagePath": "poster.jpg",
        "message": "Bean New Watch (TM)\nNew elegant watch that will help you stay in touch with your body.\nYou can see on a screen how many calories you already burn, how many steps you took\ntoday. There is a whole variety of options you'll find there.\nAks us any questions on Facebook.\n",
        "price": 74.0,
        "startDate": "2016-02-26",
        "title": "Bean New Watch",
        "type": "SPORTS"
    },
    {
        "amount": 86,
        "endDate": "2016-06-27",
        "id": 18,
        "imagePath": "poster.jpg",
        "message": "Sport Shoes for a Hot Mama.\nStay the hottest mom at the neighborhood.\nBe cool. Make them jealous!",
        "price": 21.79,
        "startDate": "2016-02-26",
        "title": "Running shoes at SportMa",
        "type": "SPORTS"
    },
    {
        "amount": 687,
        "endDate": "2018-06-25",
        "id": 19,
        "imagePath": "poster.jpg",
        "message": "New Gym at the neighborhood, \"Stay Tuned\".\nStay Tuned. New susrbers only for a dollar for month!\nWe present you a new technology for losing weight: Two really scary looking dogs\nchase you and you lose weight by escaping them!!!11\nReally effective. Recommended!",
        "price": 1.0,
        "startDate": "2016-02-26",
        "title": "Gym Stay Tuned - new membership only for a dollar!!!",
        "type": "SPORTS"
    },
    {
        "amount": 13,
        "endDate": "2017-01-01",
        "id": 20,
        "imagePath": "poster.jpg",
        "message": "Live like Harry Potter under the stairs.\nPrepare breakfast everyday for other guests who participate in Dursley experience!\n",
        "price": 59.9,
        "startDate": "2016-02-26",
        "title": "Real Harry Potter experience",
        "type": "TRAVEL"
    },
    {
        "amount": 21,
        "endDate": "2017-08-26",
        "id": 21,
        "imagePath": "poster.jpg",
        "message": "As the title suggests this is a \"trip\" to the wonderful and \"wondrous\" city of \nAmsterdam. \nMeet the White Rabbit, Cheshire Cat and other magical characters during your \"trip\"!",
        "price": 1021.0,
        "startDate": "2016-02-26",
        "title": "New experience - Alice in Wonderland",
        "type": "TRAVEL"
    }
];