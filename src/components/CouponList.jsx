var React = require('react');
var CouponTile = require('./CouponTile.jsx');
var guiStore = require('../reflux/gui_store.jsx');
var Reflux = require('reflux');


var CouponList = React.createClass({

    mixins: [Reflux.listenTo(guiStore, 'onFilterChange')],

    getInitialState: function () {
      return {filter: ''}
    },

    onFilterChange: function (event, filterQuery) {
        this.setState({
            filter: filterQuery
        })
    },
    onCouponClicked: function (coupon) {

        console.log("Calling it back!");
        this.props.childCallback(coupon);

    },

    render: function () {

        var self = this;
        return (
            <div id="couponList">
                {this.props.coupons.map(function (currentVal, index, array) {
                    currentFilter = this.state.filter.toLowerCase();
                    currentCouponTitle = currentVal.title.toLowerCase();

                    //Let's filter it out.
                    if(currentFilter != '' && !currentCouponTitle.includes(currentFilter))
                        return;
                    //If here, then no filtering is needed
                    return (<CouponTile coupon={currentVal} key={index + currentVal.id + currentVal.title} childCallBack={this.onCouponClicked}/>);

                }.bind(this))}
            </div>
        )
    }
});

module.exports = CouponList;