var React = require('react');
var NavBar = require('./NavBar.jsx');
var MainCouponView = require('./MainCouponView.jsx');
var Login = require('./Login.jsx');
var Reflux = require('reflux');
var Actions = require('../reflux/login_actions.jsx');
var loginStore = require('../reflux/login_store.jsx');

var baseUrl  = 'http://localhost:8080/api';


var MainSite = React.createClass({

    mixins : [Reflux.listenTo(loginStore, 'onChange')],
    onChange : function (event, userData) {
        this.setState({
            userData: userData
        });
        console.log(this.state.userData);
    },
    onCouponChange : function () {
          
    },
    getInitialState: function () {
        return {userData:{}};
    },
    componentWillMount : function()
    {
        Actions.getLoggedInfo();
    },

    renderTheMainSite: function () {

        return(
            <div className="whole-site">
                <NavBar userName={this.state.userData.userName}/>
                <div className="container">
                    <MainCouponView userData={this.state.userData}/>
                </div>
            </div>
        );
    },

    renderTheLoginScreen: function () {

        return (<Login/>);
    },

    render: function () {

        if(this.state.userData.logged)
            return this.renderTheMainSite();
        else
            return this.renderTheLoginScreen();
    }
});

module.exports = MainSite;