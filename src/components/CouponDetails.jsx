var React = require('react');

var CouponDetails = React.createClass({

    getInitialState : function () {

        return {
            couponToChange: this.props.coupon
        }
    },

    zeroPad: function(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    },

    onReturn: function () {
        this.state.couponToChange = this.props.coupon;
      this.props.childCallback();
    },

    renderCustomerView: function () {
        return (
            <div className="couponDetails">

                <a className="backLink" href="#" onClick={this.onReturn}><img src="img/hand.png" alt="Return to the list"/></a>
                <img src={this.props.coupon.imagePath} alt="Coupon poster image"/>
                <h1>{this.props.coupon.title}</h1>
                <div className="dateContainer">Expiration Date: <span className="date">{this.props.coupon.endDate}</span></div>
                <div className="descriptionContainer"><u className="description">{this.props.coupon.message}</u></div>
                <div className="detailsFooter"><a href="#" className="price">{this.zeroPad(this.props.coupon.price, 7)}</a></div>

            </div>
        );
    },

    /*
    Field handlers
     */
    handleFormChange: function (event) {
        console.log(event.target.getAttribute('name') + "  "  + event.target.value);

        var fieldName = event.target.getAttribute('name');
        var newValue = event.target.value;
        var changingCoupon = this.state.couponToChange;

        changingCoupon[fieldName] = newValue;

        this.setState({
            couponToChange: changingCoupon
        });
    },
    handleSaveCoupon: function () {

        console.log(`saving changes for coupon:`);
        console.log(this.state.couponToChange);
    },
    validateForm(couponToSave)
    {

        return true;
    },

    renderCompanyView: function () {

        return (
            <div className="couponDetails">

                <a className="backLink" href="#" onClick={this.onReturn}><img src="img/hand.png" alt="Return to the list"/></a>
                <img src={this.props.coupon.imagePath} alt="Coupon poster image"/>
                <h1><input type="text" name="title" onChange={this.handleFormChange} defaultValue={this.props.coupon.title}/></h1>
                <div className="dateContainer">Expiration Date: <span className="date"><input onChange={this.handleFormChange} type="text" name="endDate" onChange={this.handleFormChange} defaultValue={this.props.coupon.endDate}/></span></div>
                <div className="descriptionContainer"><textarea onChange={this.handleFormChange} name="message" cols="30" rows="10" defaultValue={this.props.coupon.message}></textarea></div>

                <div className="detailsFooter">
                    price:<input onChange={this.handleFormChange} type="text" name="price" defaultValue={this.props.coupon.price}/>
                    amount:<input onChange={this.handleFormChange} type="text" name="amount" defaultValue={this.props.coupon.amount}/>
                </div>
                <div className="detailsFooterButtons">
                    <button onClick={this.handleSaveCoupon}>Save</button>
                    <button>Remove</button>
                </div>

            </div>
        )
    },

    render: function () {

        console.log(`usertype = ${this.props.userData.userType}`);

        if(this.props.userData.userType.toLowerCase() == 'company')
            return this.renderCompanyView();
        else
           return this.renderCustomerView();
    }
});

module.exports = CouponDetails;