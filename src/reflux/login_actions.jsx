var Reflux = require('reflux');

var Actions = Reflux.createActions([
    //Login stuff

    'getLoggedInfo',
    'tryLoggingIn',
    'logout',
]);

module.exports = Actions;