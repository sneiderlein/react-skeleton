var Reflux = require('reflux');

var Actions = Reflux.createActions([
    //Login stuff

    'getCompanyCoupons',
    'createNewCoupon',
    'updateCoupon',
    'removeCoupon'
]);

module.exports = Actions;